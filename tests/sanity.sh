#!/bin/bash
#
# This is a simple sanity test to satisfy the RHEL8.1 onboard gating
# requirement.

ret=0

trqauthd --help
let ret=$ret+$?

hostn -v localhost
let ret=$ret+$?

exit $ret
